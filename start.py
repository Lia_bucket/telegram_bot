# -*- coding: utf-8 -*-
import datetime
import logging

from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters, CallbackQueryHandler
from telegram.ext import Updater

# import local variables
from configuration import *
from controllers.greetings import happy_day, happy_night, start, sticker
from controllers.messageHandler import Message, Query

# UPDATER INIT
updater = Updater(TOKEN, request_kwargs=REQUEST_KWARGS)
j = updater.job_queue
dispatcher = updater.dispatcher

# LOGGING
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

"""  HANDLERS SETUP """
start_handler = CommandHandler('start', start)
sticker = MessageHandler(Filters.sticker, sticker)
message = MessageHandler(Filters.text, Message)  # show me
query = CallbackQueryHandler(Query)
#safebooru_control = MessageHandler(Filters.text == "следующая", next_page)


""" ADD TO DISPATCHER  """
dispatcher.add_handler(start_handler)
dispatcher.add_handler(sticker)
dispatcher.add_handler(message)
dispatcher.add_handler(query)

#dispatcher.add_handler(safebooru_control)

""" METHODS THAT RUNS IN TIME  """
j.run_daily(happy_night, datetime.time(hour=15, minute=0, second=0, microsecond=0))
j.run_daily(happy_day, datetime.time(hour=2, minute=0, second=0, microsecond=0))

""" START UPDATER """
updater.start_polling()
