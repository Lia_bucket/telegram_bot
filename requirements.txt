beautifulsoup4==4.6.0
certifi==2018.4.16
chardet==3.0.4
future==0.16.0
idna==2.7
PySocks==1.6.8
python-telegram-bot==10.1.0
requests==2.19.1
telegram==0.0.1
urllib3==1.23
