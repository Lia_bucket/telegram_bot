# -*- coding: utf-8 -*-
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

from modules import safebooru

# INIT safebooru class
safebooruclass = safebooru.safebooru()
safebooru_query = "GoSafe"  # Callback query переменная


def sticker_message(bot, update, sticker_id, message):
    bot.send_sticker(chat_id=update, sticker=sticker_id)
    bot.send_message(chat_id=update, text=message)


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu


def send_list(bot, update, listing):
    print(listing)
    for i in listing:
        bot.send_photo(chat_id=update.message.chat_id, photo=i)

    if len(listing) < int(safebooruclass.params['limit']):
        sticker_message(bot, update.message.chat_id, "CAADAgADgQADWEFMCUVZCsQxTFgIAg", "Видимо больше ничего нет")


def safebooru_find(bot, update):
    tags = ''
    try:
        tags = update.message.text.lower().strip('покажи').replace(',', '')
        result = safebooruclass.search_by_tags(tags=tags)
        send_list(bot, update, result)
    except:
        sticker_message(bot, update.message.chat_id, "CAADAgADgQADWEFMCUVZCsQxTFgIAg", "Я ничего не нашла")
        return

    button_list = [
        InlineKeyboardButton(text="Вперед", callback_data=safebooru_query + "1 " + tags)
    ]

    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    bot.send_message(chat_id=update.message.chat_id, text="Controls", reply_markup=reply_markup)


def safebooru_change_page(bot, update, page, tags):
    result = safebooruclass.search_by_tags(tags=tags, current_page=page)
    button_list = [
        InlineKeyboardButton(text="Назад", callback_data=safebooru_query + str(page - 1) + " " + tags),
        InlineKeyboardButton(text="Вперед", callback_data=safebooru_query + str(page + 1) + " " + tags)
    ]
    send_list(bot, update.callback_query, result)
    if not page:  # Если мы впервые вызвали и в запросе нет страницы, то возвращаем особенный ответ
        reply_markup = InlineKeyboardMarkup(build_menu([button_list[1]], n_cols=2))
    else:  # Обычная обработка без каких-либо проблем
        reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    bot.send_message(chat_id=update.callback_query.message.chat.id, text="Controls", reply_markup=reply_markup)
