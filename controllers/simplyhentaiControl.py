# -*- coding: utf-8 -*-
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, InputMediaPhoto
from telegram.ext.dispatcher import run_async

from modules import simplyhentai
from modules.database import *

simplyhentai_query = "GoSimple"


def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu


def comic_menu(bot, chat_id, text, button_list):
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    bot.send_message(chat_id=chat_id, text=text, reply_markup=reply_markup)


def send_message(bot, chat_id, url, text):
    bot.send_photo(chat_id=chat_id, photo=url)
    bot.send_message(chat_id=chat_id, text=text)


@run_async
def hentai_find(bot, update):
    tag = update.message.text.lower().strip('порно ')
    result = simplyhentai.find_comics(tag=tag)
    print(str(result))
    insert_comic(title=result[0]['title'], href=result[0]['href'], picture=result[0]['image'])
    id = find_comic_by_href(result[0]['href'])[0][0]

    button_list = [
        InlineKeyboardButton(text="Открыть", callback_data=simplyhentai_query + "Op " + str(id)),
        InlineKeyboardButton(text="Вперед", callback_data=simplyhentai_query + "1 " + tag)
    ]
    bot.send_photo(chat_id=update.message.chat_id, photo=result[0]['image'])
    # send_message(bot, update.message.chat_id, result[0]['image'], result[0]['title'])
    comic_menu(bot, update.message.chat_id, result[0]['title'], button_list)

    # for i in result:
    #     # result[i]['href']
    #     print(i)
    #     # print(str(result[i]['image']))
    #     # print(str(result[i]['title']))
    #     send_message(bot, update, i['image'], i['title'])


@run_async
def simplyhentai_return_comic(bot, update, comic_name):
    result = simplyhentai.return_comic(comic_name)
    print(str(result))
    chunkSize = 9
    for i in range(0, len(result), chunkSize):
        chunk = [InputMediaPhoto(i) for i in result[i:i + chunkSize]]
        print(chunk)
        bot.send_media_group(update.callback_query.message.chat.id, chunk)


@run_async
def simplyhentai_change_comic(bot, update, number, tag):  # Number в этом значении обозначает номер комикса
    result = simplyhentai.find_comics(tag)

    insert_comic(title=result[number]['title'], href=result[number]['href'], picture=result[number]['image'])
    id = find_comic_by_href(result[number]['href'])[0][0]

    bot.send_photo(chat_id=update.callback_query.message.chat.id, photo=result[number]['image'])
    # send_message(bot, update.callback_query.message.chat.id, result[number]['image'], result[number]['title'])
    button_list = [
        InlineKeyboardButton(text="Назад", callback_data=simplyhentai_query + str(number - 1) + " " + tag),
        InlineKeyboardButton(text="Вперед", callback_data=simplyhentai_query + str(number + 1) + " " + tag),
        InlineKeyboardButton(text="Открыть",
                             callback_data=simplyhentai_query + "Op " + str(id))
    ]
    comic_menu(bot, update.callback_query.message.chat.id, result[number]['title'], button_list)

#
# if __name__ == "__main__":
#     hentai_find()
