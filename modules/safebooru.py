# -*- coding: utf-8 -*-
import requests
import json


class safebooru:
    def __init__(self):
        print('I\'ve been initialised')
        self.tags = ''
        self.params = {'page': 'dapi', 's': 'post', 'q': 'index', 'limit': 1, 'json': 1}

    def search_by_tags(self, tags, limit=1, current_page=0):
        list = []
        self.params['tags'] = tags
        self.params['limit'] = limit
        self.params['pid'] = current_page
        try:
            data = requests.post(url='https://safebooru.org/index.php', params=self.params)
        except:
            return False
        for i in data.json():
            list.append("http://safebooru.org/images/{0}/{1}".format(i['directory'], i['image']))
        return list

    # дописать рандом
    def search_one(self, tags, page):
        return self.search_by_tags(tags, current_page=page, limit=1)


if __name__ == "__main__":
    safebooru = safebooru()
